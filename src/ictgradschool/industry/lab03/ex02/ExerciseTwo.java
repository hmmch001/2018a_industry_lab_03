package ictgradschool.industry.lab03.ex02;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    int userA = getUserInt("Lower bound?");
    int userB = getUserInt("Upper bound?");


    private int getUserInt(String prompt){
        System.out.println(prompt);
        String userStr = Keyboard.readInput();
        int userIn = Integer.parseInt(userStr);
        return userIn;
    }


    private void start() {

        int userA = this.userA;
        int userB = this.userB;
        int userMin = Math.min(userA, userB);
        int userMax = Math.max(userA, userB);
        int a = userMin + (int) (Math.random() * ((userMax - userMin) + 1));
        int b = userMin + (int) (Math.random() * ((userMax - userMin) + 1));
        int c = userMin + (int) (Math.random() * ((userMax - userMin) + 1));
        int d = Math.max(a, b);
        int largest = Math.max(d, c);
        int e = Math.min(a, b);
        int smallest = Math.min(e, c);
        int middle = (a + b + c) - (largest + smallest);
        System.out.println("3 randomly generated numbers: " + smallest + ", " + middle + " and " + largest);
        System.out.println("Smallest number is " + smallest);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
