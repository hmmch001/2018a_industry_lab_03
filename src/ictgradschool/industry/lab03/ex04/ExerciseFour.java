package ictgradschool.industry.lab03.ex04;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        String number = getNumberFromUser();
        int decimal = getDecimalPlaces();
        truncate(number, decimal);
        String truncate = truncate(number, decimal);
        newNumber(truncate, decimal);
        }

    private String getNumberFromUser(){
        System.out.println("Please enter an amount: ");
        String number = Keyboard.readInput();
        return number;
    }

    private int getDecimalPlaces(){
        System.out.println("Please enter the number of decimal places: ");
        int decimal = Integer.parseInt(Keyboard.readInput());
        return decimal;
    }

    private String truncate(String number, int decimal){
      int dp = number.indexOf(".");
      String truncate = number.substring(0, dp + decimal + 1);
      return truncate;
    }

    private void newNumber(String truncate, int decimal){
        System.out.println("Amount truncated to " + decimal + " decimal places is: " + truncate);
    }



    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
